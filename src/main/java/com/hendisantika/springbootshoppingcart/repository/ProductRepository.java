package com.hendisantika.springbootshoppingcart.repository;

import com.hendisantika.springbootshoppingcart.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-shopping-cart
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-10
 * Time: 20:10
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findById(Long id);
}