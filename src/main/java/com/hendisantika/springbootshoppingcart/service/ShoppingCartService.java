package com.hendisantika.springbootshoppingcart.service;

import com.hendisantika.springbootshoppingcart.exception.NotEnoughProductsInStockException;
import com.hendisantika.springbootshoppingcart.model.Product;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-shopping-cart
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-10
 * Time: 21:55
 */
public interface ShoppingCartService {
    void addProduct(Product product);

    void removeProduct(Product product);

    Map<Product, Integer> getProductsInCart();

    void checkout() throws NotEnoughProductsInStockException;

    BigDecimal getTotal();
}
